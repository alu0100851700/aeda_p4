#pragma once
#include "celda.hpp"
using namespace std;

template <class clave>
class fexp{
public:
	fexp(){}
	~fexp(){}
	virtual int run(const int&, const int&, const int&) = 0;
};

template <class clave>
class felineal: public fexp<clave>{
public:

	int run(const int& nCeldas, const int& h, const int& intento){
				return (h+intento)%nCeldas;
	}

};

template <class clave>
class fecuadratica: public fexp<clave>{
public:

	int run(const int& nCeldas, const int& h, const int& intento){
				return (h+intento*intento)%nCeldas;
	}

};

template <class clave>
class fedispdoble: public fexp<clave>{
public:

	int run(const int& nCeldas, const int& h, const int& intento){
				return (h+intento)%nCeldas;
	}

};

template <class clave>
class feredisp: public fexp<clave>{
public:

	int run(const int& nCeldas, const int& h, const int& intento){
				return (h+intento)%nCeldas;
	}

};
