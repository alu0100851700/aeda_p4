#pragma once
#include "celda.hpp"
#include "fdisp.hpp"
#include "fexp.hpp"
using namespace std;

template <class clave>
class tabla{
private:
	celda<clave>** 	celda_;
	int				nCeldas_;

	fdisp<clave>*	h_;	//Funcion de dispersion
	fexp<clave>*	g_;	//Funcion de exploracion

public:
	tabla(const int& nCeldas, const int& nbloque, fdisp<clave>* h, fexp<clave>* g):
		nCeldas_(nCeldas),
		h_(h),
		g_(g)
		{

		celda_ = new celda<clave>*[nCeldas];

		for(int i=0; i<nCeldas_; i++)
			celda_[i] = new celda<clave>(nbloque); 
	}

	~tabla(void){
		delete celda_;
	}

	int h(const clave& c){
		return (h_ -> run(c, nCeldas_));
	}

	int g(const clave& c, int& intento){
		return (g_ -> run(nCeldas_, h(c), intento));
	}

	bool insertar(const clave& c){
		for(int intento=0; intento<nCeldas_; intento++){
			int pos = g(c, intento); 
			if(celda_[pos] -> empty()){
				celda_[pos] -> insertar(c);
				return true;
			}
		}

		return false;
	}

	int buscar(clave& c){
		for(int intento=0; intento<nCeldas_; intento++){
			int pos = g(c, intento); 
			if(celda_[pos] -> buscar(c))
				return pos;
			if(celda_[pos] -> empty())
				break;
		}
		return -1;
	}

	int exp_buscar(clave& c){
		int intento;
		int count=0;
		int pos_celda=0;
		for(intento=0; intento<nCeldas_; intento++){
			int pos_tabla = g(c, intento);
			pos_celda = celda_[pos_tabla] -> exp_buscar(c);

			count += pos_celda;
			if(celda_[pos_tabla] -> buscar(c))
				break;
			if(celda_[pos_tabla] -> empty())
				break;
		}
		return count;
	}

	void mostrar(void){
		for(int i=0; i<nCeldas_; i++){
			celda_[i] -> mostrar();
			cout << endl;
		}
	}
};