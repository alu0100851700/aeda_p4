#include "dni.hpp"
#include "tabla.hpp"
#include "fdisp.hpp"
#include "fexp.hpp"
#include <iostream>
using namespace std;

int main(void){
	system("clear");

	int nCeldas;
	cout << "Número de celdas: ";
	cin  >> nCeldas;

	int nBloques;
	cout << "\nTamaño de bloque: ";
	cin >> nBloques;

	fdisp<dni*>* h;
	int case_h;
	cout << "\nFuncion de dispersión:" 	<< endl
		<< "1. Modulo"					<< endl
		<< "2. Suma"					<< endl
		<< "3. Pseudo-aleatoria" 		<< endl;
	cin >> case_h;
	switch (case_h){
		case 1:
			h = new fdmodulo<dni*>;
			break;
		case 2:
			h = new fdsuma<dni*>;
			break;
		case 3:
			h = new fdaleatoria<dni*>;
			break;
		default:
			cerr << "Elección incorrecta" << endl;
			break;
	}

	fexp<dni*>* g;
	int case_g;
	cout << "\nFuncion de exploracion:" << endl
		<< "1. Lineal"					<< endl
		<< "2. Cuadratica"				<< endl
		<< "3. Dispersion doble" 		<< endl
		<< "4. Redispersion"	 		<< endl;
	cin >> case_g;
	switch (case_g){
		case 1:
			g = new felineal<dni*>;
			break;
		case 2:
			g = new fecuadratica<dni*>;
			break;
		case 3:
			g = new fedispdoble<dni*>;
			break;
		case 4:
			g = new feredisp<dni*>;
			break;
		default:
			cerr << "Elección incorrecta" << endl;
			break;
	}

	float factor;
	cout << "\nFactor de carga: ";
	cin  >> factor;

	tabla<dni*> hash(nCeldas, nBloques, h, g);

	int nRegistros = (int)(factor*nCeldas*nBloques);

	dni** a_dni = new dni*[2*nRegistros];
	for(int i=0; i<2*nRegistros; i++)
		a_dni[i] = new dni;

	for(int i=0; i< nRegistros; i++)
		hash.insertar(a_dni[i]);

	int nPruebas;
	cout << "\nNumero de pruebas: ";
	cin  >> nPruebas;

	//Información de ejecucion
	system("clear");

	cout << "Información de ejecución" << endl;

	cout << "\n\t\tCeldas\tBloques\tExploración\tCarga\tPruebas" << endl
		<< "Busquedas:\t" << nCeldas << "\t" <<  nBloques << "\t\t\t" << factor << "\t" << nPruebas << endl;


	//Estudio de la busqueda de claves en el banco de registro
	cout << "\nEstudio de la busqueda de claves que se encuentran en el banco de registro" << endl;
	int 	bMin = 0;
	int 	bMax = 0;
	float 	bMed = 0;

	for(int i=0; i<nPruebas; i++){
		int n = hash.exp_buscar(a_dni[rand()%nRegistros]);
		if(n < bMin || bMin == 0)	bMin = n;
		if(n > bMax || bMax == 0)	bMax = n;
		bMed+=n;
	}
	bMed = bMed/nPruebas;
	cout << "\t\tMinimo\tMedio\tMaximo" << endl
		<< "Busquedas:\t" << bMin << "\t" <<  bMed << "\t" << bMax << endl;

	//Estudio de la busqueda de claves en el banco de registro
	cout << "\nEstudio de la busqueda de claves que NO se encuentran en el banco de registro" << endl;
	bMin = 0;
	bMax = 0;
	bMed = 0;

	for(int i=0; i<nPruebas; i++){
		int n = hash.exp_buscar(a_dni[nRegistros+rand()%nRegistros]);
		if(n < bMin || bMin == 0)	bMin = n;
		if(n > bMax || bMax == 0)	bMax = n;
		bMed+=n;
	}
	bMed = bMed/nPruebas;

	cout << "\t\tMinimo\tMedio\tMaximo" << endl
		<< "Busquedas:\t" << bMin << "\t" <<  bMed << "\t" << bMax << endl;

	//hash.mostrar();

	
	for(int i=0; i<2*nRegistros; i++)
		delete a_dni[i];
	delete a_dni;

	cout << endl;
	return 0;
}