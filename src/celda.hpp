#pragma once
using namespace std;

template <class clave>
class celda{
private:
	clave* 	bloque_;
	int		nbloque_;

public:
	celda(const int nbloque):
		nbloque_(nbloque){

		bloque_ = new clave[nbloque_];

		//Inicializa todos los valores a nulos
		for(int i=0; i<nbloque_; i++)
			bloque_[i] = NULL;
	}

	~celda(void){
		delete bloque_;
	}

	bool buscar(clave& c) const{
		for(int i=0; i<nbloque_; i++)
			if(c == bloque_[i])	return true; 

		return false;
	}

	int exp_buscar(clave& c) const{
		int i;
		for(i=0; i<nbloque_; i++)
			if(c == bloque_[i] || bloque_[i] == NULL)	break; 

		return i+1;
	}

	bool insertar(const clave& c){
		for(int i=0; i<nbloque_; i++)
			if(bloque_[i] == NULL){
				bloque_[i] = c;
				return true;
			}

		return false;
	}

	bool empty(void) const{
		for(int i=0; i<nbloque_; i++)
			if(bloque_[i] == NULL)
				return true;

		return false;
	}

	void mostrar(void){
		for(int i=0; i<nbloque_; i++){
			if(bloque_[i] != NULL)
				cout << *bloque_[i] << "  ";
			else
				cout << "--------" << "  ";
		}
	}
};