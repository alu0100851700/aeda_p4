#pragma once
#include <cstdlib>
using namespace std;

template <class clave>
class fdisp{
public:
	fdisp(){}
	~fdisp(){}
	virtual int run(const clave&, const int&)=0;
};

template <class clave>
class fdmodulo: public fdisp<clave>{
public:

	int run(const clave& c, const int& ncelda_){
		return *c%ncelda_;
	}

};

template <class clave>
class fdsuma: public fdisp<clave>{
public:

	int run(const clave& c, const int& ncelda_){
		int d = 0;
		int x = (int)*c;

		int y;

		while(x > 0){
			y = x%10;
			d = d + y;
			x = x / 10;
		}

		return d%ncelda_;
	}

};

template <class clave>
class fdaleatoria: public fdisp<clave>{
public:

	int run(const clave& c, const int& ncelda_){

		return rand()%ncelda_;
	}

};